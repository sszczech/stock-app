var express = require('express')
, routes    = require('./routes')
, http      = require('http')
, path      = require('path')
, config    = require('./config.json')
, app       = express();

// all environments
app.set('host', config.app.HOST);
app.set('port', config.app.PORT || process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

app.locals = config.app.locals;

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

app.get('/', routes.index);

http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening http://' + app.get('host') + ':' + app.get('port'));
});
