# README #


If you want to run it locally, just follow steps below.

### How to run stock app? ###

* cd to working directory

* clone repository

```
#!bash
git clone git@bitbucket.org:sszczech/stock-app.git stock && cd stock

```


* fetch remote branches
```
#!bash
git fetch origin
git checkout -b staging origin/staging
git checkout -b develop origin/develop 

```


* install dependencies

```
#!bash
sudo npm install

```


* run app
```
#!bash
node app.js

```

* app should be accessible
[http://127.0.0.1:3000](http://127.0.0.1:3000)


You can change port of application in config.json.

If you don't have node.js or npm(node package manager) installed, install it via apt-get

```
#!bash
apt-get install python-software-properties
apt-add-repository ppa:chris-lea/node.js
apt-get update
apt-get install npm

```